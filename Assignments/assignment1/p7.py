def outer():
    def inner():
        return "Hello Core2Web"

    return inner 
    print("In Outer Function")

if __name__ == "__main__":
    res = outer()() #=> outer()=>.() 
    print(res)

