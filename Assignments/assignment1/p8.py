def outer():
    msg = "I am the outer function"

    def inner():
        return msg 

    return inner

if __name__ == "__main__":
    inner_fun = outer()
    res = inner_fun()

    print(res)
