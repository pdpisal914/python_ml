class RBI:
    name = "Reserve Bank Of India"

    def __init__(self):
        print("In RBI")

    def cashStorage():
        print("Cash Storage Of RBI")

    @classmethod
    def rulesAndRegulations(cls):
        print("Rules & Regulations of RBI")

class SBI(RBI):
    def __init__(self):
        print("In Sbi")

    def cashStorage(self):
        print("CAsh Storage : SBI")

class HDFC(RBI):

    def __init__(self):
        print("In HDFC")

    def cashStorage(self):
        print("Cash STorage : HDFC")

hdfc = HDFC()
hdfc.cashStorage()
hdfc.rulesAndRegulations()

sbi = SBI()
sbi.cashStorage()
sbi.rulesAndRegulations()


