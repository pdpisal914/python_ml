class Human:

    def __init__(self,name,age):
        print("In Human Constructor")
        self.name = name
        self.age = age

    def info(self):
        print("Name :",self.name)
        print("Age : ",self.age)

name = input("Enter The Name :")
age = int(input("Enter The Age :"))

if __name__ == "__main__":
    obj1 = Human(name,age)
    obj1.info()


