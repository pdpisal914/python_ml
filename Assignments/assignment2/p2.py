class Vehicle:
    def __init__(self,brand,model,year,speed):
        self.brand = brand
        self.model = model
        self.year = year
        self.speed = speed

    def accelerate(self):
        print("In Acceleration Function")

    def brake(self):
        print("In Brake Function")

    def honk(self):
        print("In Honk Function")

class Demo(Vehicle):
    def __init__(self,brand,model,year,speed):
        self.brand = brand
        self.model = model
        self.year = year
        self.speed = speed

    def accelerate(self):
        print("In Accelerate:Child")

    def honk(self):
        print("In Honk:Child")

Vehicle("Toyota","Fortuner",2024,180)
Demo("Suzuki","Swift",2023,10)
