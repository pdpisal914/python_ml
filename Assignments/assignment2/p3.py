class Armed_Force:
    def __init__(self):
        print("In Parent Constructor")

    @classmethod
    def security(cls):
        print("In Security Function:Armed_Force")

    @staticmethod
    def rules():
        print("In rule Fun:Armed_Forces")

class Navy(Armed_Force):
    def dressCode(self):
        print("White Dress Code")

obj = Navy()
obj.security()
obj.rules()

