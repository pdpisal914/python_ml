class A:
    def info(self):
        print("In Fun : A")
class B(A):
    def info(self):
        print("In Fun : B")
class C(A):
    def info(self):
        print("In Fun : C")
class D(A):
    def info(self):
        print("In Fun : D")
class E(D,B,C):
    def info(self):
        print("In Fun : E")
obj = E()
print(E.__mro__)

