class SBI:
    name = "State Bank Of India"

    def __init__(self,branchNo,manager,loc):
        self.manager = manager
        self.branchNo = branchNo
        self.loc = loc
        print("In SBI")

    def info(self):
        print("branch No: ",self.branchNo)
        print("Manager : ",self.manager)

    def location(self):
        print("Branch Location :",self.loc)
    
    def customers(self):
        print("all branche having different customers")

    @classmethod
    def rule(cls):
        print("Rules of SBI") 

obj = SBI(1,"Sanket Bhapkar","Baramati")
obj.info()
obj.location()
obj.customers()
obj.rule()


