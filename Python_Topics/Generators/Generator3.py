def fun():
    print("Start Fun")
    yield 10 
    yield 20
    yield 30
    print("End Fun")

ret = fun()

print(type(ret))
print(type(fun))

for i in ret:
    print(i+10)
