def outer():
    def inner1():
        print("In inner1 Fun")

    def inner2():
        print("In inner2 Fun")
    
    return inner1,inner2

retObj = outer()
print(retObj)

inn1,inn2 = outer()
inn1()
inn2()


for i in retObj:
    i()
