def outer():
    def inner():
        print("In Inner Fun")

    inner()
    print("In Outer Fun")

print("Start Code")
outer()
print("End Code")
