def outer(x,y):
    def inner():
        print("In Inner Fun")

    print("In Outer Fun")
    return inner

retObj = outer(10,20)
retObj()
