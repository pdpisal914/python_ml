def outer(x,y):
    def inner(a,b):
        print("In Inner Fun")
        return a+b

    print("In Outer Fun")
    print(x+y)
    return inner

retObj = outer(5,8)
innerRet = retObj(20,30)
print(retObj)
