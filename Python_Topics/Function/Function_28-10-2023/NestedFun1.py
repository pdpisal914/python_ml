def outer():
    def inner():
        print("In Inner Fun")

    print("In outer Fun")

    def inner2():
        print("In Inner2 Fun")

    inner()
    inner2()

outer()
