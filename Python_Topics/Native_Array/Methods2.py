import array as arr
listData = [10,20,30,40]
arrData = arr.array('i',[100,200,300,400,100])

print(arrData)
arrData.fromlist(listData)

print(arrData)

print(arrData.index(400))

arrData.insert(4,500)
print(arrData)

arrData.pop()
print(arrData)

arrData.remove(100)

print(arrData)

arrData.reverse()

print(arrData)

cpList = arrData.tolist()
