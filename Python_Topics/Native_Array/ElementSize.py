import array as arr
data = arr.array('i',[10,20,30])
print(data.itemsize)

data = arr.array('u',['A','B','C'])
print(data.itemsize)
data = arr.array('h',[10,20,30])
print(data.itemsize)

data = arr.array('f',[10.5,20.0,30.0])
print(data.itemsize)
data = arr.array('q',[10,20,30])
print(data.itemsize)
data = arr.array('d',[10,20,30])
print(data.itemsize)


