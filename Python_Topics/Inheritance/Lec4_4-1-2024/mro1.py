class Parent1:
    def __init__(self):
        print("In Parent 1")

    def dispdata(self):
        print("In disp data")

class Parent2:
    def printData(self):
        print("In Print data")

class Child(Parent2,Parent1):
    pass


obj = Child()
obj.dispdata()
obj.printData()
print(Child.mro())


