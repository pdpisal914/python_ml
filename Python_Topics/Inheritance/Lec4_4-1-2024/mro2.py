class A:
    def fun(self):
        print("In Fun :A")

class B:
    def fun(self):
        print("In Fun :B")

class C:
    def fun(self):
        print("In Fun :C ")

class D(A,B):
    def fun(self):
        print("IN Fun :D")
        super().fun()

class E(B,C):
    def fun(self):
        print("In Fun :E ")
        super().fun()

class F(D,E):
    def fun(self):
        print("In Fun :F ")
        super().fun()


obj = F()
obj.fun()

print(F.mro())

