class Parent1:
    def dispData(self):
        print("In Disp Data")

class Parent2:
    def printData(self):
        print("In Print data")


class Child(Parent1,Parent2):
    pass


obj = Child()
obj.dispData()
obj.printData()


