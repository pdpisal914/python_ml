class type:
    def __init__(self,x):
        print("Hello")


class Parent1:
    def __init__(self):
        print("In Parent1")

class Parent2:
    def __init__(self):
        print("In Parent2")

class Child(Parent1,Parent2):
    def __init__(self):
        print("In Child Class")

obj = Child()

print(type(obj))
