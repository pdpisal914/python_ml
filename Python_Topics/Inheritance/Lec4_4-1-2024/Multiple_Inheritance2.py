class Parent1:
    def __init__(self):
        print("In Parent1")

    def dispData(self):
        print("In Disp data")

class Parent2:
    def __init__(self):
        print("In Parent2")

    def printData(self):
        print("In Print Data")

class Child(Parent1,Parent2):
    def __init__(self):
        print("In Child")


obj = Child()
obj.dispData()
obj.printData()
