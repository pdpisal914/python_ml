class Parent{
	static int x = 10;
	Parent(){
		System.out.println("X in Parent Const. :"+x);
	}
}
class Child extends Parent{
	Child(){
		System.out.println(x);
		System.out.println(Parent.x);
		System.out.println(Child.x);
	}
}
class Demo{
	public static void main(String [] pdp){
		Parent obj = new Child();
	}
}

