class Parent:

    def __init__(self):
        print("in Patrent Constructor")

    def parentFun(self):
        print("In Parent Fun")

class Child(Parent):

    def __init__(self):
        pass


    def childFun(self):
        print("In Child fun");


obj = Child()
obj.parentFun()
obj.childFun()
