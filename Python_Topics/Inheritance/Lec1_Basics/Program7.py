class Parent:
    x = 10
    y = 20

    def __init__(self):
        print("In Parent Constructor")

    def parentFun(self):
        print(self.x)
        print(self.y)

class Child(Parent):

    def __init__(self):
        print(Child.x)
        print(Parent.y)

    def childParent(self):
        print(Child.x)
        print(Child.y)

obj1 = Child()
obj1.parentFun()
obj1.childParent()
