class Parent:

    def __init__(self):
        print("Self In Parent :",id(self))

class Child(Parent):

    def __init__(self):
        Parent.__init__(super)
        print("Self in Child :", id(self))
        print("super In Child :",id(super))


obj = Child()
