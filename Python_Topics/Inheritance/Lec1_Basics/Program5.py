class Parent:

    def __init__(self):
        print(id(self))

    def parentFun(self):
        print("In Parent Fun")

class Child(Parent):

    def __init__(self):
        super().__init__()

        print("Self : ",id(self))
        print(id(Parent))
        print(id(super()))
        print(id(super))

obj = Child()
obj.parentFun()
