class Parent:

    def __init__(self):
        self.x = 10
        self.y = 20

    def setVar(self,z):
        self.z = z

    def parentFun(self):
        print(self.x)
        print(self.y)
        print(self.z)

class Child(Parent):
    pass

obj1 = Child()
obj1.parentFun()
