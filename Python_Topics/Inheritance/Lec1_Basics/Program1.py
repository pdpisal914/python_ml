
class Parent:
    def __init__(self):
        print("In Parent Constructor")

    def parentFun(self):
        print("In Parent Fun")

class Child(Parent):
    pass


obj1 = Child()
obj2 = Child()

print(id(obj1))
print(id(obj2))
