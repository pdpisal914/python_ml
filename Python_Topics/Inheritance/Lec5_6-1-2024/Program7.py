class Boss:
    def report(self):
        print("In Boss report")

class Manager1(Boss):
    def report(self):
        print("In Manager 1 report")

class Manager2(Boss):
    def report(self):
        print("In Manager 2 report")

class Manager3(Boss):
    def report(self):
        print("In Manager 3 report")

class TeamLead1(Manager1,Manager3):
    def report(self):
        print("In TeamLead 1 report")


class TeamLead2(Manager2,Manager3):
    def report(self):
        print("In TeamLead 2 report")

class Developer(TeamLead1,TeamLead2):
    def report(self):
        print("In Developer report")

obj = Developer()
obj.report()
print(dir(obj))
print(dir(Developer))
print(Developer.__mro__)

