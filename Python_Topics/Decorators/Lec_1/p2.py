def outer():
    print("In Outer function")

    def innerFun1():
        print("In Inner Function 1")

    def innerFun2():
        print("In Inner Function 2")

    return innerFun1,innerFun2


ret1,ret2 = outer()
ret1()
ret2()
