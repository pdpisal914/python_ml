def outer():
    print("In Outer Function")
        
    def innerFun1():
            print("In Inner Function 1")

    def innerFun2():
            print("In Inner Function 2")

    innerFun1()
    innerFun2()


outer()
