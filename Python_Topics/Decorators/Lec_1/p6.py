#Decorator


def decorFun(func):
    print("In Decor Function")

    def wrapper():
        print("Start Wrapper")

        func()

        print("End Wrapper")

    return wrapper

def normalFun():
        print("In Normal Function")

normalFun = decorFun(normalFun)
normalFun()
