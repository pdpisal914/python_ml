
def decorFun(func):
    print("In DecorFun");

    def wrapper():
        print("Start Wrapper 2")

        func()

        print("End Wrapper 2")

    return wrapper

def decorRun(func):
    print("In DecorRun");

    def wrapper():
        print("Start wrapper 1")

        func()

        print("End Wrapper 1")

    return wrapper


def normalFun():
        print("IN Normal Function")

normalFun = decorFun(decorRun(normalFun))
normalFun()
