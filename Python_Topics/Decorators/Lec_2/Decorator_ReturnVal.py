

def decorFun(func):
    print("In Decor Function")

    def wrapper(*args):
        print("Start Wrapper")

        func(args)
        #func(*args)

        print("End Wrapper")

    return wrapper


def normalFun(x,y):
    print("In Normal Function")
    return x+y

#normalFun = decorFun(normalFun)
print( normalFun(10,20) ) 
