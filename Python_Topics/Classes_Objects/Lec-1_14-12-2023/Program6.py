#Class Variable  /  Class Variable

class Employee:

    z = 10


    def __init__(self):
        print("In Constructor")
        self.x = 30
        self.y = 40

    def disp(self):
        print(self.x)
        print(self.y)

obj = Employee()
obj1 = Employee()
obj.disp()
print(obj)
print("obj.z= ",hex(id(obj.z)))
print("Employee.z = ",hex(id(Employee.z)))

obj.z = 50
print("obj.z= ",hex(id(obj.z)))
print("Employee.z = ",hex(id(Employee.z)))


print("obj1.z= ",hex(id(obj1.z)))
print("Employee.z = ",hex(id(Employee.z)))

