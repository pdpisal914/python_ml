#The methods inside the class is not accessible from outside of the code without creating Object

class company(object):
    def disp():
        print("Display")

def fun():
    print("In Fun")

print("Start Code")
fun()
disp()   #Name Error
print("End Code")

