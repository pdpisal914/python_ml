#What is happen when we create object

class Employee:
    def __init__(self):
        print("In Constructor")
        self.x=10   #Instance Variable
        self.y=20   #Instance Variable

    def disp(self):
        print(self.x)
        print(self.y)

obj=Employee()
obj.disp()
print(type(obj.disp))
