#Class Variable  /  Class Variable

class Employee:

    z = 10
    t = 20

    def __init__(self):
        print("In Constructor")
        self.x = 30
        self.y = 40

    def disp(self):
        print(self.x)
        print(self.y)

obj = Employee()
obj1 = Employee()
obj.disp()
print(obj)
print("obj = ",id(obj))
print("obj.z= ",id(obj.z))
print("Employee.z = ",id(Employee.z))
print("obj.t= ",id(obj.t))
print("Employee.t = ",id(Employee.t))


print("obj1 = ",id(obj1))
print("obj1.z= ",id(obj1.z))
print("Employee.z = ",id(Employee.z))
print("obj1.t= ",id(obj1.t))
print("Employee.t = ",id(Employee.t))
