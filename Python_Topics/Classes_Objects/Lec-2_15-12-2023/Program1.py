
#Pass Parameters from constructor


class Employee:

    def __init__(self,empId,empName):
        print("In Constructor")
        self.empId = empId
        self.empName = empName

    def disp(self):
        print(self.empId)
        print(self.empName)


emp1 = Employee(12,"Kanha")
emp1.disp()

emp2 = Employee(15,"Rahul")
emp2.disp()
